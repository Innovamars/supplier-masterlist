// Optional atributes means that it appears on National o International form only

export class Supplier {
    // Metadata
    id: string;
    date: string;
    national: boolean;
    chemical: boolean;

    // First block: General information
    generalInfo: SupplierInformation;

    // Seccond block: Transport information
    transport?: SupplierTransport;

    // Third block: Contact information
    generalManager: SupplierContact;
    qualityManager: SupplierContact;
    accountsReceivable: SupplierContact;
    sales: SupplierContact;

    // Fourht block: Finatial information
    bankingInfo: SupplierBanking;

    // Fifht block: Quality system
    qualitySystem?: SupplierQuality;

    // Internal use block
    internalUse: SupplierInternal;

    // Internal data
    color: string; // --> Mostar
}

class SupplierInformation {
    name: string; // --> Mostar
    identifier?: string;
    phone: string; // --> Mostar
    address: string;
    city: string; // --> Mostar
    country?: string;
    web: string; // --> Mostar
    industry: string; // --> Mostar
    description: string;
}

class SupplierTransport {
    shipment: string;
    transportation?: string;
    chemicalHandling: boolean;
}

class SupplierContact {
    name: string;
    email: string;
    phone: string;
    mobile: string;
}

class SupplierBanking {
    // International
    swift?: string;
    aba?: string;
    bankAddress?: string;
    paycheckAddress?: string;
    city?: string;
    state?: string;
    zip?: string;

    // National
    identifier?: string;

    accountName: string; // Beneficiario
    accountNumber: string;
    bankName: string;
}

class SupplierQuality {
    iso: QualityQuestion;
    department: QualityQuestion;
    certificates: QualityQuestion;
    corrective: QualityQuestion;
    retention: string;
    keep: QualityQuestion;
    complaints: QualityQuestion;
    audit: QualityQuestion;
    evaluation: QualityQuestion;
    traceability: QualityQuestion;
    fakes: QualityQuestion;
    establish: QualityQuestion;
    additional: string;
}

class QualityQuestion {
    isTrue: boolean;
    comment: string;
}

class SupplierInternal {
    services: InternalClassification[];
    evaluation: QualityQuestion;
    audit: QualityQuestion;
    national: QualityQuestion;
}

class InternalClassification {
    family: string;
    classfication: string;
    quality: string;
    fulfill: boolean;
    comments: string;
}
