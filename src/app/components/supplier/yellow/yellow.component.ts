import { Component, OnInit } from '@angular/core';
import { Supplier } from '../../../models/supplier';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-yellow',
  templateUrl: './yellow.component.html',
  styleUrls: ['./yellow.component.css']
})
export class YellowComponent implements OnInit {

  private suppliersCollection: AngularFirestoreCollection<Supplier>;
  suppliers: Observable<Supplier[]>;

  constructor(private readonly afs: AngularFirestore) {
    this.suppliersCollection = this.afs.collection<Supplier>('suppliers', ref => ref.where('color', '==', 'yellow'));
    this.suppliers = this.suppliersCollection.valueChanges();

    // Agrega el ID obtenido de Firestore a la instancia del objeto como un nuevo atributo
    // this.suppliers = this.suppliersCollection.snapshotChanges().map(actions => {
    //   return actions.map(a => {
    //     const data = a.payload.doc.data() as Supplier;
    //     const id = a.payload.doc.id;
    //     return { id, ...data };
    //   });
    // });
  }

  ngOnInit() {
    // this.getSuppliers().subscribe(actionArray => {
    //   actionArray.map(item => {
    //     const id = this.afs.createId();
    //     const data = item.payload.doc.data() as Supplier;
    //     data.id = id;
    //     this.afs.collection<Supplier>('requests').doc(id).set(data);
    //   });
    // });
  }

  deleteSupplier(supplier: Supplier | string) {
    const id = typeof supplier === 'string' ? supplier : supplier.id;
    this.suppliersCollection.doc(id).delete();
  }

  getSuppliers() {
    return this.afs.collection('suppliers').snapshotChanges();
  }
}
