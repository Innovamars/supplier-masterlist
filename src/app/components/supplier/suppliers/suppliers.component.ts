import { Component, OnInit, ViewChild } from '@angular/core';
import { Supplier } from '../../../models/supplier';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable, BehaviorSubject } from 'rxjs';
import {MatPaginator, MatTableDataSource} from '@angular/material';



@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

//DataSource para la tabla
dataSource1 = new MatTableDataSource();
displayedColumns1: string[]=['name','phone','city','web' ,'industry','color','buttonShow','buttonDelete'];
/**/



@ViewChild(MatPaginator) paginator: MatPaginator;




  private suppliersCollection: AngularFirestoreCollection<Supplier>;
  suppliers: Observable<Supplier[]>;
  // color: string;
  colors = [
    'all',
    'green',
    'yellow',
    'red'
  ];

  constructor(private readonly afs: AngularFirestore) {


    
    this.suppliersCollection = this.afs.collection<Supplier>('suppliers');
    this.suppliers = this.suppliersCollection.valueChanges();


    // this.suppliersCollection = this.afs.collection<Supplier>('suppliers', ref => {
    //   let query: firebase.firestore.Query = ref;
    //   if (this.color) { query = query.where('color', '==', this.color); }
    //   return query;
    // });
    // this.suppliers = this.suppliersCollection.valueChanges();

    // Agrega el ID obtenido de Firestore a la instancia del objeto como un nuevo atributo
    // this.suppliers = this.suppliersCollection.snapshotChanges().map(actions => {
    //   return actions.map(a => {
    //     const data = a.payload.doc.data() as Supplier;
    //     const id = a.payload.doc.id;
    //     return { id, ...data };
    //   });
    // });
  }

  ngOnInit() {

    this.suppliers.subscribe(supliers => {
      this.dataSource1.data = supliers as Supplier[];
      this.dataSource1.paginator = this.paginator;
    })
    // this.filterByColor('all');
    // this.getSuppliers().subscribe(actionArray => {
    //   actionArray.map(item => {
    //     const id = this.afs.createId();
    //     const data = item.payload.doc.data() as Supplier;
    //     data.id = id;
    //     this.afs.collection<Supplier>('requests').doc(id).set(data);
    //   });
    // });
  }

  deleteSupplier(supplier: Supplier | string) {
    const id = typeof supplier === 'string' ? supplier : supplier.id;
    this.suppliersCollection.doc(id).delete();
  }

  getSuppliers() {
    return this.afs.collection('suppliers').snapshotChanges();
  }

  applyFilter(filterValue: string) {
    this.dataSource1.filterPredicate = function(data : Supplier, filter: string): boolean {
      return data.color.toLowerCase().includes(filter);
  };
    this.dataSource1.filter = filterValue.trim().toLowerCase();
  }




  filterByColor(color: string|null) {
    if (color === 'all') {
      color = null;
    }
    this.suppliersCollection = this.afs.collection<Supplier>('suppliers', ref => {
      let query: firebase.firestore.Query = ref;
      if (color) { query = query.where('color', '==', color); }
      return query;
    });
    this.suppliers = this.suppliersCollection.valueChanges();
  }
}
