import { Component, OnInit } from '@angular/core';
import { Supplier } from '../../../models/supplier';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  private suppliersCollection: AngularFirestoreCollection<Supplier>;
  suppliers: Observable<Supplier[]>;

  constructor(private readonly afs: AngularFirestore) {
    this.suppliersCollection = this.afs.collection<Supplier>('requests');
    this.suppliers = this.suppliersCollection.valueChanges();
  }

  ngOnInit() { }

  acceptSupplier(supplier: Supplier | string) {
    const id = typeof supplier === 'string' ? supplier : supplier.id;
    this.suppliersCollection.doc(id).valueChanges().subscribe(document => {
      // console.log(document);
      this.afs.collection<Supplier>('suppliers').doc(id).set(document);
    });
    this.suppliersCollection.doc(id).delete();
  }

  deleteSupplier(supplier: Supplier | string) {
    const id = typeof supplier === 'string' ? supplier : supplier.id;
    this.suppliersCollection.doc(id).delete();
  }

  getSuppliers() {
    return this.afs.collection('requests').snapshotChanges();
  }
}
