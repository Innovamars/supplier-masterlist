import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndetailsComponent } from './endetails.component';

describe('EndetailsComponent', () => {
  let component: EndetailsComponent;
  let fixture: ComponentFixture<EndetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
