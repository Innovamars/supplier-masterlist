import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Encom3Component } from './encom3.component';

describe('Encom3Component', () => {
  let component: Encom3Component;
  let fixture: ComponentFixture<Encom3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Encom3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Encom3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
