import { Component, OnInit, Input } from '@angular/core';
import { Supplier } from '../../../../models/supplier';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-esdetails',
  templateUrl: './esdetails.component.html',
  styleUrls: ['./esdetails.component.css']
})

export class EsdetailsComponent implements OnInit {

  //Codigo para pasar parametros desde otro componente
  //@Input() supplier: Supplier;

  colors = ['green', 'yellow', 'red'];

  private supplierCollection: AngularFirestoreCollection<Supplier>;
  //supplier: Observable<Supplier>;
  supplier: Supplier =  new Supplier();

  formMaster = this.fb.group({
    id: [''],
    chemical: [true],
    color: ['green'],
    // First block
    generalInfo : this.fb.group({
      name: ['', Validators.required],
      identifier: [''],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      country: [''],
      web: ['', Validators.required],
      industry: ['', Validators.required],
      description: ['', Validators.required]
    }),
    // Second block
    transport : this.fb.group({
      shipment: [''],
      transportation: ['', Validators.required],
      chemicalHandling: ['', Validators.required]
    }),
    // Third block
    generalManager: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    qualityManager: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    accountsReceivable: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    sales: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    // Fourth block
    bankingInfoGeneral: this.fb.group({
      accountName: ['', Validators.required], // Beneficiario
      accountNumber: ['', Validators.required],
      bankName: ['', Validators.required]
    }),

    bankingInfoNational: this.fb.group({
      identifier: ['']
    }),

    bankingInfoInternational: this.fb.group({
      swift: [''],
      aba: [''],
      bankAddress: [''],
      paycheckAddress: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),

    // Fifth block
    qualitySystem: this.fb.group({
      iso: this.fb.group({isTrue: [''], comment: ['']}),
      department: this.fb.group({isTrue: [''], comment: ['']}),
      certificates: this.fb.group({isTrue: [''], comment: ['']}),
      corrective: this.fb.group({isTrue: [''], comment: ['']}),
      retention: [''],  // str
      keep: this.fb.group({isTrue: [''], comment: ['']}),
      complaints: this.fb.group({isTrue: [''], comment: ['']}),
      audit: this.fb.group({isTrue: [''], comment: ['']}),
      evaluation: this.fb.group({isTrue: [''], comment: ['']}),
      traceability: this.fb.group({isTrue: [''], comment: ['']}),
      fakes: this.fb.group({isTrue: [''], comment: ['']}),
      establish: this.fb.group({isTrue: [''], comment: ['']}),
      additional: [''] // str
    }),

    // Internal use
    internalUse: this.fb.group({
      services: this.fb.array([
        this.fb.group({
          family: [''],
          classification: [''],
          quality: [''],
          fulfill: [''],
          comments: ['']
        })
      ]),
      evaluation: this.fb.group({isTrue: [''], comment: ['']}),
      audit: this.fb.group({isTrue: [''], comment: ['']}),
      national: this.fb.group({isTrue: [''], comment: ['']})
    })

  });

  constructor(private route: ActivatedRoute,
              private location: Location,
              private fb: FormBuilder,
              private afs: AngularFirestore) { }

  ngOnInit() {
    this.supplierCollection = this.afs.collection<Supplier>('suppliers');
    this.supplierCollection.doc(this.getId).valueChanges().subscribe(sup => {
      this.supplier = sup as Supplier;
      this.formMaster.patchValue(this.supplier);
    });
  }

  onSubmit() {
    console.warn(this.formMaster.value);
    this.supplierCollection.doc(this.getId).set(this.formMaster.value);
    Swal.fire("Guardado correctamente");
  }


  get getId() {
    return this.route.snapshot.paramMap.get('id');
  }

  get isChemical() {
    return this.formMaster.get('chemical').value;
  }

    // Form Array
  get internalServices() {
      return this.formMaster.get('internalUse').get('services') as FormArray;
  }

  addInternalUse() {
    this.internalServices.push(
      this.fb.group({
      family: [''],
      classification: [''],
      quality: [''],
      fulfill: [''],
      comments: ['']
    }));
  }
}
