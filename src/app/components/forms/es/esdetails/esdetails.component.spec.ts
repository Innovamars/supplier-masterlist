import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsdetailsComponent } from './esdetails.component';

describe('EsdetailsComponent', () => {
  let component: EsdetailsComponent;
  let fixture: ComponentFixture<EsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
