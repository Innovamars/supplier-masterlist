import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators} from '@angular/forms';
import { Supplier } from '../../../../models/supplier';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-escom3',
  templateUrl: './escom3.component.html',
  styleUrls: ['./escom3.component.css']
})
export class Escom3Component implements OnInit {
  private suppliersCollection: AngularFirestoreCollection<Supplier>;

  formMaster = this.fb.group({
    chemical: [true],
    color: ["green"],
    // First block
    generalInfo : this.fb.group({
      name: ['', Validators.required],
      identifier: [''],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      country: [''],
      web: ['', Validators.required],
      industry: ['', Validators.required],
      description: ['', Validators.required]
    }),
    // Second block
    transport : this.fb.group({
      shipment: [''],
      transportation: ['', Validators.required],
      chemicalHandling: ['', Validators.required]
    }),
    // Third block
    generalManager: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    qualityManager: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    accountsReceivable: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    sales: this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: [''],
      mobile: ['']
    }),
    // Fourth block
    bankingInfoGeneral: this.fb.group({
      accountName: ['', Validators.required], // Beneficiario
      accountNumber: ['', Validators.required],
      bankName: ['', Validators.required]
    }),

    bankingInfoNational: this.fb.group({
      identifier: ['']
    }),

    bankingInfoInternational: this.fb.group({
      swift: [''],
      aba: [''],
      bankAddress: [''],
      paycheckAddress: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),

    // Fifth block
    qualitySystem: this.fb.group({
      iso: this.fb.group({isTrue: [''], comment: ['']}),
      department: this.fb.group({isTrue: [''], comment: ['']}),
      certificates: this.fb.group({isTrue: [''], comment: ['']}),
      corrective: this.fb.group({isTrue: [''], comment: ['']}),
      retention: [''],  // str
      keep: this.fb.group({isTrue: [''], comment: ['']}),
      complaints: this.fb.group({isTrue: [''], comment: ['']}),
      audit: this.fb.group({isTrue: [''], comment: ['']}),
      evaluation: this.fb.group({isTrue: [''], comment: ['']}),
      traceability: this.fb.group({isTrue: [''], comment: ['']}),
      fakes: this.fb.group({isTrue: [''], comment: ['']}),
      establish: this.fb.group({isTrue: [''], comment: ['']}),
      additional: [''] // str
    }),

    // Internal use
    internalUse: this.fb.group({
      services: this.fb.array([
        this.fb.group({
          family: [''],
          classification: [''],
          quality: [''],
          fulfill: [''],
          comments: ['']
        })
      ]),
      evaluation: this.fb.group({isTrue: [''], comment: ['']}),
      audit: this.fb.group({isTrue: [''], comment: ['']}),
      national: this.fb.group({isTrue: [''], comment: ['']})
    })

  });

  constructor(private fb: FormBuilder, private readonly afs: AngularFirestore) {
    this.suppliersCollection = this.afs.collection<Supplier>('suppliers');
  }

  ngOnInit() { }

  onSubmit() {
    console.warn(this.formMaster.value);
    this.addSupplier(this.formMaster.value);
  }

  addSupplier(supplier: Supplier) {
    // Este codigo solo agrega un ID personalizado al documento
    const id = this.afs.createId();
    supplier.id = id;
    this.suppliersCollection.doc(id).set(supplier);

    // Agrega un ID generado por Firebase y lo agrega el los atributos del objeto
    // this.suppliersCollection.add(supplier)
    // .then(ref => {
    //   ref.set({ id: ref.id }, { merge: true });
    // });
  }

  // Form Array
  get internalServices() {
      return this.formMaster.get('internalUse').get('services') as FormArray;
  }

  addInternalUse() {
    this.internalServices.push(
      this.fb.group({
      family: [''],
      classification: [''],
      quality: [''],
      fulfill: [''],
      comments: ['']
    }));
  }

  get isChemical() {
    return this.formMaster.get('chemical').value;
  }
}
