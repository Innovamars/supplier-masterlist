import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Escom3Component } from './escom3.component';

describe('Escom3Component', () => {
  let component: Escom3Component;
  let fixture: ComponentFixture<Escom3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Escom3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Escom3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
