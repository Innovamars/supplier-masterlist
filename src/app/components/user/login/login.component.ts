import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  returnParam = 'returnUrl';

  mensaje = '';
  email: string;
  password: string;

  returnUrl: string;

  constructor(private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    // reset login status
    // this.auth.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams[this.returnParam] || '/';
  }

  login(email: string, password: string) {
    this.auth.login(email, password)
      .then(data => {
        Swal.fire({
        position: 'top-end',
        type: 'success',
        title: '',
        showConfirmButton: false,
        timer: 1000
        });
        // login successful so redirect to return url
        this.router.navigateByUrl(this.returnUrl);
    }).catch(err => {
        Swal.fire(
          'Verifique email y contraseña!',
          '',
          'error'
          );
        });
  }
}
