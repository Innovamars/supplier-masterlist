import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { Page404Component } from './components/page404/page404.component';
import { GreenComponent } from './components/supplier/green/green.component';
import { RedComponent } from './components/supplier/red/red.component';
import { YellowComponent } from './components/supplier/yellow/yellow.component';
import { LoginComponent } from './components/user/login/login.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { Escom3Component } from './components/forms/es/escom3/escom3.component';
import { Encom3Component } from './components/forms/en/encom3/encom3.component';
import { ConfigurationComponent} from './components/configuration/configuration.component';
import { ReportsComponent} from './components/reports/reports.component';
import { RequestsComponent} from './components/forms/requests/requests.component';
import { EsdetailsComponent} from './components/forms/es/esdetails/esdetails.component';
import { EndetailsComponent} from './components/forms/en/endetails/endetails.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { FormsMenuComponent } from './components/forms/forms-menu/forms-menu.component';
import { SuppliersComponent } from './components/supplier/suppliers/suppliers.component';

const routes: Routes = [
  { path: '', redirectTo: '/suppliers', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'suppliers', component: SuppliersComponent, canActivate: [AuthGuard] },
  { path: 'suppliers/green', component: GreenComponent, canActivate: [AuthGuard] },
  { path: 'suppliers/red', component: RedComponent, canActivate: [AuthGuard] },
  { path: 'suppliers/yellow', component: YellowComponent, canActivate: [AuthGuard] },
  { path: 'en/com3', component: Encom3Component, canActivate: [AuthGuard] },
  { path: 'es/com3', component: Escom3Component, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'configuration', component: ConfigurationComponent, canActivate: [AuthGuard] },
  { path: 'report', component: ReportsComponent, canActivate: [AuthGuard] },
  { path: 'request', component: RequestsComponent, canActivate: [AuthGuard] },
  { path: 'es/detail/:id', component: EsdetailsComponent, canActivate: [AuthGuard] },
  { path: 'en/detail', component: EndetailsComponent, canActivate: [AuthGuard] },
  { path: 'forms', component: FormsMenuComponent, canActivate: [AuthGuard] },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'login', component: LoginComponent },
  { path: '404', component: Page404Component },
  { path: '**', redirectTo: '/404', pathMatch: 'full'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
