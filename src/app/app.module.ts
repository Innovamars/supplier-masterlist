import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Modulos Agregados para Tabla y Formulario
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatRadioModule} from '@angular/material/radio';


import { AppComponent } from './app.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { LoginComponent } from './components/user/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { GreenComponent } from './components/supplier/green/green.component';
import { RedComponent } from './components/supplier/red/red.component';
import { Page404Component } from './components/page404/page404.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { YellowComponent } from './components/supplier/yellow/yellow.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';


import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatSlideToggleModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Escom3Component } from './components/forms/es/escom3/escom3.component';
import { Encom3Component } from './components/forms/en/encom3/encom3.component';

import { WelcomeComponent } from './components/welcome/welcome.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { EsdetailsComponent } from './components/forms/es/esdetails/esdetails.component';
import { EndetailsComponent } from './components/forms/en/endetails/endetails.component';
import { RequestsComponent } from './components/forms/requests/requests.component';
import { ConfigurationComponent } from './components/configuration/configuration.component';
import { ReportsComponent } from './components/reports/reports.component';
import { FormsMenuComponent } from './components/forms/forms-menu/forms-menu.component';
import { SuppliersComponent } from './components/supplier/suppliers/suppliers.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LoginComponent,
    HomeComponent,
    GreenComponent,
    RedComponent,
    Page404Component,
    NavbarComponent,
    YellowComponent,
    SidenavComponent,
    WelcomeComponent,
    Escom3Component,
    Encom3Component,
    EsdetailsComponent,
    EndetailsComponent,
    RequestsComponent,
    ConfigurationComponent,
    ReportsComponent,
    FormsMenuComponent,
    SuppliersComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,

    FormsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatRadioModule
  ],
  providers: [AngularFirestore,{ provide: FirestoreSettingsToken, useValue: {} }],
  bootstrap: [AppComponent]
})
export class AppModule { }
